package no.ntnu.tdt4250.primes;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import no.ntnu.tdt4250.primes.PrimeGenerator;

class PrimeGeneratorTest {

	private static PrimeGenerator pg;
	
	@BeforeAll
	static void initialize() {
		pg = new PrimeGenerator();
	}
	
	@Test
	void testZeroSize() {
		assertEquals(0, pg.generatePrimes(0).length);
	}
	
	@Test
	void testFirstPrimeShouldBe2() {
		int[] primes = pg.generatePrimes(1);
		assertEquals(2, primes[0]);
	}
	
	@Test
	void testFirst10Primes() {
		int[] primes = pg.generatePrimes(10);
		assertArrayEquals(new int[] { 2,3,5,7,11,13,17,19,23,29 }, primes);
	}
	
	@Test
	void testVectorSize() {
		int[] primes = pg.generatePrimes(100);
		assertEquals(100, primes.length);
	}

	@Test 
	void testFirst10PrimesFrom13() {
		int[] primes = pg.generatePrimes(10,13);
		assertArrayEquals(new int[] { 13,17,19,23,29,31,37,41,43,47 }, primes);
	}
	
	@Test 
	void testStartingNumberLongSequence() {
		int[] primes100 = pg.generatePrimes(100);
		int[] primes90from30 = pg.generatePrimes(90,30);
		
		for(int k=0; k < primes90from30.length; k++) {
			assertEquals(primes100[k+10],primes90from30[k]);
		}
	}

}
