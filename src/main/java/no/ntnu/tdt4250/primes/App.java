package no.ntnu.tdt4250.primes;

import joptsimple.OptionParser;
import joptsimple.OptionSet;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	// Defining possible options (using JOptSimple syntax)
    	// A letter means that options is possible, a colon (:)
    	// means it accepts a parameter
    	OptionParser parser = new OptionParser("n:k:s");
    	
    	// Parse the options from command line arguments of the program
    	OptionSet options = parser.parse(args);
    	
    	// Default values: find first 10 primes starting from 0
    	int n = 10;
    	int k = 0;
    	
    	// If the -n option is specified use its value
    	if(options.has("n")) {
    		n = Integer.parseInt(options.valueOf("n").toString());
    	}
    	// If the -k option is specified use its value
    	if(options.has("k")) {
    		k = Integer.parseInt(options.valueOf("k").toString());
    	}
    	
        PrimeGenerator gen = new PrimeGenerator();
        int[] nums = gen.generatePrimes(n,k);

    	// If the -p option is specified print the numbers
        if(!options.has("s")) {
        	for(int prime : nums) {
        		System.out.println(prime);
        	}
        }
    }
}
