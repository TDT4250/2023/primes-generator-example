package no.ntnu.tdt4250.primes;

public class PrimeGenerator {

	public int[] generatePrimes(int n) {
		return generatePrimes(n, 0);
	}
	
	public int[] generatePrimes(int n, int start) {
		int[] primes = new int[n];
			
		int i = 0;
		// Hardcode 2 as prime if starting number is less than 3
		if(start < 3 && i < n) {
			primes[i++] = 2;
		}
		// Hardcode 3 as prime if starting number is less than 5
		if(start < 5 && i < n) {
			primes[i++] = 3;
			start = 5;
		}
		
		int nextCandidate = start;
		// The first candidate should be odd
		// (all even numbers are for sure not prime)
		if(nextCandidate % 2 == 0) {
			nextCandidate++;
		}
		
		// Iterate on the vector (n)
		for(; i<primes.length; i++) {
		
			boolean found = false;
	
			// Iterate on possible candidates until the
			// next prime number is found. Candidates
			// are all even number
			for(int k=nextCandidate; !found; k=k+2) {
				boolean divisible = false;
				// Check parity -- should never match
				if(k % 2 == 0) {
					divisible = true;
				}
				// Iterate on possible divisors
				for(int div=3; div < (k/2) && !divisible; div=div+2) {
					if(k % div == 0) {
						divisible = true;
					}
 				}
				if(!divisible) {
					found = true;
					primes[i] = k;
				}
			}
			
			// The next candidate is the next odd number
			nextCandidate = primes[i] + 2;
		}
		return primes;		
	}
}
